package controller

func (pc *ProxyCtrl) HasProxySer(path string) bool {
	pr := pc.rep
	serId, _ := pc.parsePath(path)
	return pr.HasSer(serId)
}
